from django.apps import AppConfig


class TrelloapiConfig(AppConfig):
    name = 'trelloapi'
