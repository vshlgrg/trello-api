from django.db import models
import uuid
from django.utils import timezone

class Board(models.Model):
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    owner= models.ForeignKey('trelloapi.User', on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
    created_date = models.DateTimeField(
                    auto_now_add=True)
    modified_date = models.DateTimeField(
                    auto_now=True)

    def __str__(self):
        return self.name

class User(models.Model):
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    username = models.CharField(max_length=50)
    email = models.CharField(max_length=50)

    def __str__(self):
        return self.username

class TaskList(models.Model):
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=50)
    description = models.TextField()
    board_id = models.ForeignKey('trelloapi.Board', on_delete=models.CASCADE)
    created_date = models.DateTimeField(
                    default=timezone.now)
    modified_date = models.DateTimeField(
                    default=timezone.now)
    def __str__(self):
        return self.name


class Card(models.Model):
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=50)
    description = models.TextField()
    tasklist_id = models.ForeignKey('trelloapi.TaskList', on_delete=models.CASCADE)
    created_date = models.DateTimeField(
                    default=timezone.now)
    modified_date = models.DateTimeField(
                    default=timezone.now)

    def __str__(self):
        return self.name

