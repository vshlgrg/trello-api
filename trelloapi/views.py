from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from .models import User, Board, TaskList, Card
from .serializers import UserSerializer, BoardSerializer, TaskListSerializer, CardSerializer

class UserView(APIView):
    
    def get(self, request, uuid=None):
        if uuid == None:
            users = User.objects.all()
        else:
            users = User.objects.all().filter(uuid=uuid)

        serial = UserSerializer(users, many=True)
        return Response(serial.data)

    def post(self, request):
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid(): 
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=400)

    def patch(self, request, uuid):
        user = User.objects.all().get(uuid=uuid)
        serializer = UserSerializer(user, data=request.data, partial=True)
        if serializer.is_valid(): 
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=400)



class UserDetailView(APIView):

    def get(self, request, uuid):
        board = Board.objects.all().filter(owner=uuid)
        serial = BoardSerializer(board, many=True)
        return Response(serial.data)



class BoardView(APIView):

    def get(self, request, uuid=None):
        if uuid == None:
            boards = Board.objects.all()
        else:
            boards = Board.objects.all().filter(uuid=uuid)

        serial = BoardSerializer(boards, many=True)
        return Response(serial.data)

    def post(self, request):
        serializer = BoardSerializer(data=request.data)
        if serializer.is_valid(): 
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=400)

    def patch(self, request, uuid):
        board = Board.objects.all().get(uuid=uuid)
        serializer = BoardSerializer(board, data=request.data, partial=True)
        if serializer.is_valid(): 
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=400)


class BoardDetailView(APIView):

    def get(self, request, uuid):
        tasklist = TaskList.objects.all().filter(board_id=uuid)
        serial = TaskListSerializer(tasklist, many=True)
        return Response(serial.data)


class TaskListView(APIView):

    def get(self, request, uuid=None):
        if uuid == None:
            lists = TaskList.objects.all()
        else:
            lists = TaskList.objects.all().filter(uuid=uuid)

        serial = TaskListSerializer(lists, many=True)
        return Response(serial.data)

    def post(self, request):
        serializer = TaskListSerializer(data=request.data)
        if serializer.is_valid(): 
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=400)

    def patch(self, request, uuid):
        tasklist = TaskList.objects.all().get(uuid=uuid)
        serializer = TaskListSerializer(tasklist, data=request.data, partial=True)
        if serializer.is_valid(): 
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=400)


class TaskListDetailView(APIView):

    def get(self, request, uuid):
        card = Card.objects.all().filter(tasklist_id=uuid)
        serial = CardSerializer(card, many=True)
        return Response(serial.data)



class CardView(APIView):

    def get(self, request, uuid=None):
        if uuid == None:
            cards = Card.objects.all()
        else:
            cards = Card.objects.all().filter(uuid=uuid)

        serial = CardSerializer(cards, many=True)
        return Response(serial.data)

    def post(self, request):
        serializer = CardSerializer(data=request.data)
        if serializer.is_valid(): 
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=400)


    def patch(self, request, uuid):
        card = Card.objects.all().get(uuid=uuid)
        serializer = CardSerializer(card, data=request.data, partial=True)
        if serializer.is_valid(): 
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=400)
