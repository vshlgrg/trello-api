from django.urls import path, re_path
from trelloapi import views


urlpatterns = [
    path('user/', views.UserView.as_view()),
    re_path('user/(?P<uuid>[\w\-]+)$', views.UserView.as_view()),
    re_path('user/(?P<uuid>[\w\-]+)/board$', views.UserDetailView.as_view()),

    path('board/', views.BoardView.as_view()),
    re_path('board/(?P<uuid>[\w\-]+)$', views.BoardView.as_view()),
    re_path('board/(?P<uuid>[\w\-]+)/tasklist$', views.BoardDetailView.as_view()),


    path('tasklist/', views.TaskListView.as_view()),
    re_path('tasklist/(?P<uuid>[\w\-]+)$', views.TaskListView.as_view()),
    re_path('tasklist/(?P<uuid>[\w\-]+)/card$', views.TaskListDetailView.as_view()),


    path('card/', views.CardView.as_view()),
    re_path('card/(?P<uuid>[\w\-]+)$', views.CardView.as_view()),
]
