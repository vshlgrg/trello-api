from django.contrib import admin
from .models import User, Card, Board, TaskList

# Register your models here.
admin.site.register(User)
admin.site.register(Card)
admin.site.register(Board)
admin.site.register(TaskList)
