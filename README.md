
# Trello API sample usage

## ~ Resources ~
| Resource | URL |
| -------- |-----|
|All Users| http://localhost:8000/trello/user/|
|All Cards| http://localhost:8000/trello/card/|
|All Boards| http://localhost:8000/trello/board/ |
|Al Tasks| http://localhost:8000/trello/task/ |
|Single user| http://localhost:8000/trello/<uuid of user>/ |
|Single card| http://localhost:8000/trello/<uuid of card>/ |
|Single board| http://localhost:8000/trello/<uuid of board>/ |
|Single tasklist| http://localhost:8000/trello/<uuid of tasklist>/ |
|User owned boards| http://localhost:8000/trello/user/<uuid of user>/board |
|Tasklists inside a board| http://localhost:8000/trello/board/<uuid of board>/task |
|Cards inside a Tasklist| http://localhost:8000/trello/tasklist/<uuid of tasklist>/card |

---
##### User

`GET` http://localhost:8000/trello/user/


Get all users
```json
[
    {
        "uuid": "7ffa6a7d-ff5f-4695-b8fb-41500e9aa83b",
        "username": "garg.v",
        "email": "garg.v@media.net"
    },
    {
        "uuid": "1667452b-9e57-4dc8-8d7a-1336be6e62c2",
        "username": "looper",
        "email": "looper.re@media.net"
    }
]
```

Get specific user


`GET` http://localhost:8000/trello/user/1667452b-9e57-4dc8-8d7a-1336be6e62c2


`GET` http://localhost:8000/trello/user/<uuid>

OUTPUT
```JSON
[
    {
        "uuid": "1667452b-9e57-4dc8-8d7a-1336be6e62c2",
        "username": "looper",
        "email": "looper.re@media.net"
    }
]
```


`POST` http://localhost:8000/trello/user/


Create new instances
```json
{
        "username": "garg.v",
        "email" : "garg.v@media.net"
}
```

`PATCH` http://localhost:8000/trello/user/ 
Do partial updates
```json
{
        "username": "v.garg"
}
```

-----

##### Card
data fields
```json
{
    name: "Beginnings",
    description: "This is the end.",
    tasklist_id: "e9515a21-7261-4a5c-9dcd-a2f65b68bb7e",
    
}
```

##### Tasklist
data fields
```json
{
    name: "Origin",
    description: "This is origins.",
    board_id: "e9515a21-7261-4a5c-9dcd-a2f65b68bb7e",
    
}
```

##### Board
data fields
```json
{
    name: "Genesis",
    owner: "7ffa6a7d-ff5f-4695-b8fb-41500e9aa83b"
    
}
```